package com.example.stylescope.ui.designers

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.stylescope.R

class DesignersActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_designers)
    }
}